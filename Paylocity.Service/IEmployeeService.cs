﻿using Paylocity.Business;
using System.Collections.Generic;

namespace Paylocity.Service
{
    public interface IEmployeeService
    {
        List<Employee> GetEmployees();
        Employee GetById(int id);
    }
}
