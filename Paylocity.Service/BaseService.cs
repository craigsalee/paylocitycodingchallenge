﻿using Paylocity.DataAccess;
using System;

namespace Paylocity.Service
{
    public abstract class BaseService : IDisposable
    {
        protected readonly DbPaylocityEntities context;

        protected BaseService()
        {
            context = new DbPaylocityEntities();
        }

        public void Dispose()
        {
            context?.Dispose();
        }

    }
}
