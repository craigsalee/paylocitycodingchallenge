﻿using AutoMapper;
using Paylocity.Business;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Employee = Paylocity.Business.Employee;

namespace Paylocity.Service
{
    public class EmployeeService : BaseService, IEmployeeService
    {
        private readonly IMapper mapper;

        public EmployeeService()
        {
            var config = new MapperConfiguration(delegate (IMapperConfigurationExpression cfg)
            {
                cfg.CreateMap<DataAccess.Employee, Employee>();
                cfg.CreateMap<Employee, DataAccess.Employee>();
                cfg.CreateMap<EmployeeDependent, DataAccess.EmployeeDependent>();
                cfg.CreateMap<DataAccess.EmployeeDependent, EmployeeDependent>();
            });
            mapper = config.CreateMapper();
        }

        public List<Employee> GetEmployees()
        {
            var employees = context.Employees.ToList();
            var businessEmployee = mapper.Map<IList<Employee>>(employees).ToList();
            return businessEmployee;
        }

        public Employee GetById(int id)
        {
            var employee = context.Employees.Include(ed => ed.EmployeeDependents).FirstOrDefault(e => e.EmployeeId == id);
            var businessEmployee = mapper.Map<Employee>(employee);
            return businessEmployee;
        }

        public int Create(Employee employee)
        {
            var entity = mapper.Map<DataAccess.Employee>(employee);
            context.Employees.Add(entity);
            return context.SaveChanges();
        }

        public int CreateDependent(EmployeeDependent dependent)
        {
            var entity = mapper.Map<DataAccess.EmployeeDependent>(dependent);
            var employee = context.Employees.FirstOrDefault(e => e.EmployeeId == dependent.EmployeeId);
            if (employee != null)
            {
                employee.EmployeeDependents.Add(entity);
                return context.SaveChanges();
            }
            return -1; //TODO: Add exception handling and throw here.  Also, add logging throughout app.
        }
    }
}
