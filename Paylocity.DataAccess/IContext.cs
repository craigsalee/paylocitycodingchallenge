﻿using System.Data.Entity;

namespace Paylocity.DataAccess
{
    public interface IContext
    {
        DbSet<Employee> Employees { get; set; }
        DbSet<EmployeeDependent> EmployeeDependents { get; set; }

        int SaveChanges();
        void Dispose();
        void Dispose(bool disposing);
    }
}
