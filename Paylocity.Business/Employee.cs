﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Paylocity.Business
{
    public class Employee
    {
        public Employee()
        {
            //This would need to be refactored once requirements grew.
            PaychecksPerYear = 26;
            YearlyBenefitCost = 1000;
        }

        public int EmployeeId { get; set; }

        [Required]
        //Unique in database
        public string EmployeeNbr { get; set; }

        [Required]
        [StringLength(100)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(100)]
        public string LastName { get; set; }

        public ICollection<EmployeeDependent> EmployeeDependents { get; set; }

        public int PaychecksPerYear { get; set; }

        public Decimal YearlyBenefitCost { get; set; }

        public Decimal TotalYearlyBenefitsCost()
        {
            Decimal dependentCost = 0;
            if (EmployeeDependents != null)
                dependentCost = EmployeeDependents.Sum(d => d.YearlyBenefitCost);

            return dependentCost + YearlyBenefitCost;
        }

        public Paycheck EstimatePaycheck()
        {
            var yearlyBenefitsCost = TotalYearlyBenefitsCost();
            Decimal paycheckBenefitsCost = 0;
            if (yearlyBenefitsCost > 0)
                paycheckBenefitsCost = yearlyBenefitsCost / PaychecksPerYear;

            Paycheck paycheck = new Paycheck
            {
                EmployeeId = this.EmployeeId,
                TotalDeductions = paycheckBenefitsCost
            };
            return paycheck;
        }
    }
}
