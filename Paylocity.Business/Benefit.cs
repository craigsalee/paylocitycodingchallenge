﻿namespace Paylocity.Business
{
    public class Benefit
    {
        public int EmployeeId { get; set; }
        public int BenefitTypeId { get; set; }
    }

    //enum Benefit Types = Would be in static table
}
