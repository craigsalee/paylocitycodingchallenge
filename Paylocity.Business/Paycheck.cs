﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Paylocity.Business
{
    public class Paycheck
    {
        public int EmployeeId { get; set; }

        [DataType(DataType.Currency)]
        [DisplayFormat(DataFormatString = "{0:C}")]
        [DisplayName("Gross Pay")]
        public Decimal GrossPay { get; set; }

        [DataType(DataType.Currency)]
        [DisplayFormat(DataFormatString = "{0:C}")]
        [DisplayName("Net Pay")]
        public Decimal NetPay
        {
            get { return GrossPay - TotalDeductions; }
            set { throw new NotImplementedException(); }
        }

        //Refactor to store a list of Deductions with deduction types and amounts
        [DataType(DataType.Currency)]
        [DisplayFormat(DataFormatString = "{0:C}")]
        [DisplayName("Total Deductions")]
        public Decimal TotalDeductions { get; set; }


        public Paycheck()
        {
            GrossPay = 2000;
        }
    }
}
