﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Paylocity.Business
{
    public class EmployeeDependent
    {
        public EmployeeDependent()
        {
            YearlyBenefitCost = 500;
        }

        public int EmployeeDependentId { get; set; }

        [Required]
        [ForeignKey("Employee")]
        public int EmployeeId { get; set; }

        [Required]
        public Employee Employee { get; set; }

        [Required]
        [StringLength(100)]
        public string FirstName { get; set; }

        [Required]
        [StringLength(100)]
        public string LastName { get; set; }

        public Decimal YearlyBenefitCost { get; set; }

    }
}
