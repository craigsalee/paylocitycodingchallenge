﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Paylocity.Business;
using PayrollSample.Controllers;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace PayrollSample.Tests.Controllers
{
    [TestClass]
    public class EmployeeControllerTest
    {

        [TestMethod]
        public void Index()
        {
            // Arrange
            EmployeeController controller = new EmployeeController();

            // Act
            ViewResult result = controller.Index() as ViewResult;

            var model = result.Model as List<Employee>;

            // Assert
            Assert.AreEqual("Craig", model.First().FirstName);
        }

    }
}
