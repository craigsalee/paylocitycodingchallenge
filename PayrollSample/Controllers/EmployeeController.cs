﻿using Paylocity.Business;
using Paylocity.Service;
using System.Web.Mvc;


namespace PayrollSample.Controllers
{
    public class EmployeeController : Controller
    {

        // GET: Employee
        public ActionResult Index()
        {
            EmployeeService svc = new EmployeeService();
            return View(svc.GetEmployees());
        }

        // GET: Employee/Details/5
        public ActionResult Details(int id)
        {
            EmployeeService svc = new EmployeeService();
            return View(svc.GetById(id));
        }


        // GET: Employee/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Employee/Create
        [HttpPost]
        public ActionResult Create(Employee employee)
        {
            try
            {
                EmployeeService svc = new EmployeeService();
                svc.Create(employee);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }


        // GET: Employee/Create
        public ActionResult CreateDependent(int id)
        {
            EmployeeDependent viewData = new EmployeeDependent() { EmployeeId = id };
            return View(viewData);
        }


        // POST: Employee/Create
        [HttpPost]
        public ActionResult CreateDependent(EmployeeDependent dependent)
        {
            try
            {
                EmployeeService svc = new EmployeeService();
                svc.CreateDependent(dependent);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }




    }
}
