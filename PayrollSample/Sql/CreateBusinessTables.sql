﻿CREATE TABLE [dbo].[Employee]
(
	[EmployeeId] INT IDENTITY (1, 1) NOT NULL PRIMARY KEY,
	[EmployeeNbr] nvarchar(100) NOT NULL,
	[FirstName] nvarchar(100) NOT NULL,
	[LastName] nvarchar(100) NOT NULL
)

alter table [Employee] add constraint AK_EmployeeNbr unique([EmployeeNbr]);


CREATE TABLE [dbo].[EmployeeDependent]
(
	[EmployeeDependentId] INT IDENTITY (1, 1) NOT NULL PRIMARY KEY,
	[EmployeeId] INT NOT NULL,
	[FirstName] nvarchar(100) NOT NULL,
	[LastName] nvarchar(100) NOT NULL
)


ALTER TABLE [EmployeeDependent]
ADD CONSTRAINT FK_EmployeeId
FOREIGN KEY (EmployeeId) REFERENCES Employee(EmployeeID);


