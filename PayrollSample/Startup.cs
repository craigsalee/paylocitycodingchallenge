﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(PayrollSample.Startup))]
namespace PayrollSample
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
